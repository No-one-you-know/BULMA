-- This is the configuration file of the BULMA bot, it's a lua file so it's easy
-- to load it as a module and have it across the whole program without getting
-- the global space dirty. 

local config = {} -- DON'T TOUCH THIS LINE

config.DATABASE_ENGINE = "SQLITE3"  -- available: SQLITE, SQLITE3, MYSQL, POSTGRES, OCI8 (Oracle)
config.DATABASE_FILE_LOCATION = "DB/" -- Only applies for sqlite
config.DATABASE_NAME = "BULMA"
config.DEFAULT_AMOUNT_SUBMISSIONS_SHOW = 15
config.DEFAULT_SUBMISSIONS_REPORT_SCHEME = "['problem.name:30'] | ['problem.contestId''problem.index'] | ['problem.tags'] | ['problem.link']"

return config
