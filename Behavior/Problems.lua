local Problems = require('GeneralBehavior'):new()
local GS = require('GeneralStuff')
local util = require('Utilities')

local luadate = require 'date'; luadate.fmt("%D %H:%M:%S") -- set the default presentation format

local Instances = Instances -- This avoids messing the global variable itself a bit

Problems.HELPTEXT = table.concat({
"PROBLEMS:  Shows the problems available for a online judge.",
"    NOT IMPLEMENTED YET."
}, "\n")

Problems.FIELDS =
[[contestId             | ID of the contest of a problem
index                 | Index of a problem
name                  | Name of a problem
points                | Amount of points a problem is worth
tags                  | Tags belonging to a problem
type                  | PROGRAMMING, QUESTION
link                  | Link to a problem's webpage
solvedCount           | Amount of people that has solved a problem
]]

Problems.timeUnits = util.timeUnits
Problems.defaultReportScheme = 
"['contestId''index'] | ['name'] | ['tags'] | ['link'] | solved: 'solvedCount'"

local fieldMethods_mt =  -- Metatable for Problems.fieldMethods
{ __index = function(self)  return self['_default']  end }

Problems.fieldMethods = {
	["_default"] = Problems._defaultTransform,
	["solvedCount"] = function(num) return num and (num < 0) and "?" or num end
}

setmetatable( Problems.fieldMethods, fieldMethods_mt )

function Problems:init( map )
	if type(map) == 'table' then
		self.headersMap = map
	end
end

function Problems:Run()
	local args = GS.getArguments( self.headersMap.text )
	
	local send = function( text )
		if text then
			self.headersMap.text = text
		end
		return GS.sendMessage( GS.forgeMessage( self.headersMap ))
	end
	
	if type(args) == "nil" or ( type(args) == "table" and args[0] < 1 ) then
		send( "NO ARGS WERE PASSED" )
		return
	end
	
	if args["--help"] then  -- Show help text
		send( self.HELPTEXT )
		return
	elseif args["--listfields"] then -- Show the available fields
		send( self.FIELDS )
		return
	end
	
	local judge = (type(args["--judge"])=="string" and args["--judge"]:lower()) or
			(type(args["--oj"]) == "string" and args["--oj"]:lower())  or  nil
	local amount = args["--amount"] or "15" -- 15 problems to show by default
	local tags = args["--tags"]
	local problemIds = args["--problems"]
	local groupBy = args["--groupby"] -- by tags, by contestId ... 
	local scheme = args["--show"]	-- To tweak the report scheme
	local sendJSON = args["--json"]
	local fetch = args["--fetch"]
	
	
	-- ###########################################################################
	-- Check for some conditions about the arguments being passed
	-- JUDGE
	if type(judge) ~= "string" then
		send( "Missing or invalid judge name" )
		return nil
	elseif not Instances.JUDGES[judge] then
		send( "No such online judge" )
		return nil
	
	-- AMOUNT
	elseif type(amount) ~= "string" or util.tonumber(amount) < -1 or util.tonumber(amount) == 0 then
		send( "Invalid amount passed" )
		return nil
	
	-- TAGS
	elseif tags and type(tags) ~= "string" then
		send( "Invalid tags datatype, expected string and got "..type(tags) )
		return nil
	
	-- SCHEME (REPORT)
	elseif scheme and type(scheme) ~= "string" then
		send( "Invalid report scheme (--show) datatype passed, expected string, got " .. type(scheme) )
		return nil
	
	-- PROBLEM IDs
	elseif problemIds and type("problemIds") ~= "string" then
		send( "Invalid list of problem IDs passed, expected a string, got " .. type(problemIds) )
		return nil
	
	end
	-- ###########################################################################
	
	
	-- Set default values for the not passed arguments so all arguments can be
	-- passed forward without issues
	amount = util.tonumber(amount) == -1 and math.pow(2,32) or util.tonumber(amount) 
	problemIds = problemIds and util.split(problemIds, '%s*,%s*')
	tags = tags and util.split(tags, '%s*,%s*')
	
	local probs, status
	local update = false
	if fetch then 
		probs, status = Instances.JUDGES[judge]:getProblems(
			{
				tags = tags,
				problemIds = problemIds
			}
		)
		update = true
	else
		local dbAccess = GS.newDatabaseAccess()
		probs, status = dbAccess:getProblems{
			tags = tags,
			problemIds = problemIds
		}
		dbAccess:close()
	end
		
	if status == "OK" then
		if update then
			local dbAccess = GS.newDatabaseAccess()
			dbAccess:insertProblems( probs )
			dbAccess:close()
		end
		
		local quantity = math.min( #probs, amount )
		local reportScheme
		
		if type(scheme) == "string" then
			reportScheme = Problems.createProblemReportStructure(scheme)
		end
		
		if groupBy then
			local groupingFields = util.split(groupBy, "%s*,%s*")
			for i = 1, #groupingFields do
				probs = Problems.groupProblems( probs, groupingFields[i] )
			end
		end
		
		if quantity > 0 then
			if sendJSON then
				send( GS.objectToJson( probs ) )
			else
				send( 
					table.concat{
						(Problems.makeReport(probs, quantity, reportScheme)),
						"The amount of shown problems is: ",
						quantity
					}
				)
			end
		else
			send( "NO PROBLEMS TO SHOW." )
		end
	else
		send( "AN ERROR OCURRED - " .. (status or "") )
	end
	
end

function Problems.groupProblems( probs, field )
	if type(probs) ~= "table" then return {} end
	
	-- Check if field name is empty
	if util.trim(field):len() <= 0 then
		return probs, "Empty field name for grouping."
	end
	
	-- Check if there are problems to group
	if (type(probs._amount) == "number" and probs._amount <= 0) and #probs <= 0 then
		return probs, "There are no problems to group."
	end
	
	-- Check briefly if the field does exists
	if not probs._isGrouped_ and not util.getTableField(probs[1], field) then
		return probs, table.concat{"The field '", field, "' does not exist."}
	end
	
	local groupedProbs = {}
	if probs._isGrouped_ then
		groupedProbs._groupedByField_ = probs._groupedByField_
		
		local quant = 0
		for index, value in pairs( probs ) do
			if type(value) == 'table' then
				groupedProbs[index] = Problems.groupProblems(value, field)
				quant = quant + (groupedProbs[index]._amount or #groupedProbs[index])
			end
		end
		
		groupedProbs._amount = quant
	else
		local tmp
		
		for i = 1, #probs do			-- For each problem
			tmp = util.getTableField( probs[i], field)
			
			if tmp == nil then
				tmp = "No " .. field
			end
			
			if not groupedProbs[ tmp ] then
				groupedProbs[ tmp ] = {}
			end
			
			groupedProbs[ tmp ][ #groupedProbs[tmp] + 1 ] = probs[i]
		end
		
		groupedProbs._amount = #probs
		groupedProbs._groupedByField_ = field
	end
	
	groupedProbs._isGrouped_ = true
	return groupedProbs
end

function Problems.makeReport( problems, remaining, scheme, nest_lvl )
	nest_lvl = type(nest_lvl) == 'number' and nest_lvl or 0
	
	if type(scheme) == 'nil' then
		scheme = Problems.createProblemReportStructure(
			Problems.defaultReportScheme
		)
	end
	
	local output = {scheme.headers, "\n"}
	local tabulation = ("  "):rep( nest_lvl )
	local processedProblems = 0
	
	if problems._isGrouped_ then
		local answer, processed
		for key, val in pairs( problems ) do

			if type(val) == 'table' then
				table.insert( output, tabulation )
				table.insert( output, '[' )
				table.insert( output, problems._groupedByField_ )
				table.insert( output, ': ' )
				table.insert( output, tostring(key) )
				table.insert( output, ']\n' )
				
				answer, processed = 
					Problems.makeReport(val, remaining, scheme, nest_lvl + 1)
				
				table.insert( output, answer )
				
				processedProblems = processedProblems + processed
				remaining = remaining - processed
				
				if remaining <= 0 then  break  end
			end
		end
	else
	
		for i = 1, math.min( #problems, remaining ) do
			table.insert( output, tabulation )
			
			table.insert( output, 
				Problems.describeProblem( problems[i], scheme ) )
			
			processedProblems = processedProblems + 1
		end
	end
	
	return table.concat(output), processedProblems
end

function Problems.createProblemReportStructure( requiredFieldsToShow )
	local scheme = { headers = requiredFieldsToShow }
	local preffix, field, suffix
	local keepRaw, fieldName, maxLength
	
	while requiredFieldsToShow:len() > 0 do
		preffix, field, suffix = requiredFieldsToShow:match( "^([^']*)'?([^']*)'?(.*)$" )
		requiredFieldsToShow = suffix
		
		table.insert( scheme, preffix )
		
		keepRaw, fieldName, maxLength = util.trim(field):match( "^(%*?)([^:]+):?([0-9]*)$" )
		keepRaw = type(keepRaw) == "string" and string.len(keepRaw) > 0 and true
		
		if type(fieldName) == 'nil' or 
		   type(fieldName) == 'string' and string.len(fieldName) <= 0 then
			break
		end
		
		if type(maxLength) == 'string' then
			maxLength = util.tonumber(maxLength)
		else
			maxLength = nil
		end
		
		scheme[#scheme+1] = { 
			['field'] = fieldName, 
			['maxLength'] = maxLength,
			['method'] = keepRaw and Problems.fieldMethods["_default"] or
				Problems.fieldMethods[fieldName]
		}
	end
	
	return scheme
end

function Problems.describeProblem( problem, scheme )
	local description = {}
	
	if type(scheme) == 'nil' then
		scheme = Problems.createProblemReportStructure(
			Problems.defaultReportScheme
		)
	end
	
	local tmp
	for i = 1, #scheme do
		if type(scheme[i]) == 'string' then
			tmp = scheme[i]
		else
			tmp = util.getTableField( problem, scheme[i].field )
			
			if type(tmp) == 'table' then
				-- Just concats numeric values or strings that are put in a 
				-- 1-based array fashion, separated by ", "
				local first = true
				local str = {}
				for j = 1, #tmp do
					if type(tmp[j]) == 'number' or type(tmp[j]) == 'string' then
						if first then
							first = false
						else
							table.insert( str, ", " ) 
						end
						table.insert( str, tmp[j] )
					end
				end
				tmp = table.concat( str )
			else
				-- Apply the transformations being holded in scheme[i].method 
				tmp = scheme[i].method( tmp )
			end
			
			if type(scheme[i].maxLength) == 'number' then
				tmp = tmp:sub( 1, scheme[i].maxLength )
			end
		end
		
		table.insert( description, tmp )
	end
	
	table.insert( description, '\n' )
	return table.concat( description )
end

return Problems
