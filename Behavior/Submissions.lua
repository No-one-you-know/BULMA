local Submissions = require('GeneralBehavior'):new()
local util = require('Utilities')
local GS = require('GeneralStuff')

local luadate = require 'date'; luadate.fmt("%D %H:%M:%S") -- set the default presentation format

local conf = GS.configuration
local Instances = Instances -- This avoids messing the global variable itself a bit

Submissions.HELPTEXT = table.concat({
"SUBMISSIONS: Shows submissions of an user filtered by tags, etc.",
"--help",
"    Shows this help text.\n",
"--by USERNAME [, USERNAME, ...]",
"    Fetch submissions having one of this usernames as it's author.\n",
"--oj JUDGE, --judge JUDGE",
"    From this online judge.\n",
"--since <number>[s|mi|h|d|w|m|y]",
"    Look for submissions from TIME ago.\n",
"--amount QUANTITY",
"    Show at most QUANTITY submissions. Pass -1 to show all of them.\n",
"--tags TAG [, TAG, ...]",
"    Show submissions matching at least one of this tags.\n",
"--verdicts VERDICT [, VERDICT, ...]",
"    Show submissions matching at least one of this verdicts.\n",
"--groupby FIELD [, FIELD, ...]",
"    Groups the submissions based on the fields passed, in order.\n",
"--order oldest|newest",
"    Sorts the submissions by it's creation time, newest first by default.\n",
"--problems ID [, ID, ...]",
"    Shows only the problems with the ID matching the IDs passed.\n",
"--submissions ID [, ID, ...]",
"    Shows only the submissions with the ID matching the IDs passed.\n",
"--show FIELD[:len] [,FIELD[:len], ...]",
"    Allows you to limitedly select what submission's info are being shown.\n",
"--listfields",
"    Shows the possible fields to group submissions by.\n",
"--timeintervals INTERVAL [, INTERVAL , ...]",
"    Filter the submissions by time.",
"\n",
"FIELD",
"  This represents a field inside a submission structure that can be used to gather many submissions\
 under one or more common values, e.g. : group by 'verdict', 'problem.name' will group the submissions by verdict\
 that being, grouping the ones that go under 'AC', then the ones under 'WA', etc ... and then for each verdict,\
 group by the problem name.  The -len- is the maximum length for that field, mind that numbers may be trimmed\
 rendering them invalid and useless.\
 If a * is put right before the fieldname then those values will be showed as they were received from the judge:\
      '*creationTimeSeconds'\
 this will show as the number of seconds instead of a date.\n",
"INTERVAL",
"  Is a representation of an interval of time used for filtering the submissions, it's description goes like:\n\
      ([<date>|<time lapse>][,|::][<date>|<time lapse>])\n\
 where <time lapse> is a number followed by any of ('s','mi','h','d','w','m','y') and\
 date is a formatted date as MM/DD/YYYY [HH:MM:SS], starting from the epoch (01/01/1970 00:00:00)\
 some examples are:\n\
      (4y,15m)   This sets an interval that goes 'from 4 years ago up to 15 months ago'\n\
      (7m::2m)   This is 'a lapse of time of 2 months starting from 7 months ago'\n\
      ('04/01/2012','04/01/2012 12:45:00')  'First 12 hours and 45 minutes of April 1st, 2012'\n\
 note that a lapse of time must not contain a date as it's second parameter, e.g: (17m::'02/01/2013') is invalid."
}, "\n")

Submissions.GROUPBYFIELDS =
[[author.contestId              |
author.ghost                  |
author.members                |
author.participantType        |
author.startTimeSeconds       |
contestId                     | ID of the contest of a submission
creationTimeSeconds           | Time in seconds when the submissions was created
id                            |
memoryConsumedBytes           | Amount of memory used by a submission when judged
passedTestCount               | Amount of test passed
problem.contestId             | ID of the contest of a problem
problem.index                 | Index of a problem
problem.name                  | Name of a problem
problem.points                | Amount of points a problem is worth
problem.tags                  | Tags belonging to a problem
problem.type                  | PROGRAMMING, QUESTION
problem.link                  | Link to a problem's webpage
programmingLanguage           | Programming language used for a submission
relativeTimeSeconds           | Time in seconds since the start of a contest
testset                       |
timeConsumedMillis            | Amount of milliseconds taken to run a submission
verdict                       | Final judgement result, tells if passed or not
fullVerdictName               | Same as verdict but with the full name being displayed
]]

Submissions.timeUnits = util.timeUnits
Submissions.defaultReportScheme = conf.DEFAULT_SUBMISSIONS_REPORT_SCHEME

local fieldMethods_mt =  -- Metatable for Submissions.fieldMethods
{ 	__index = function(self)  return self['_default']  end  }

Submissions.fieldMethods = -- This maps the functions above and respective fields
{
	['creationTimeSeconds']     = Submissions._secondsToDate,
	['author.startTimeSeconds'] = Submissions._secondsToDate,
	['relativeTimeSeconds']     = Submissions._secondsToDate,
	['memoryConsumedBytes']     = Submissions._betterSizeInBytes,
	['_default']                = Submissions._defaultTransform
}
setmetatable( Submissions.fieldMethods, fieldMethods_mt )

function Submissions:init( map )
	if type(map) == 'table' then
		self.headersMap = map
	end
end

function Submissions:Run()
	local args = GS.getArguments( self.headersMap.text ) 
	
	local send = function( text )
		if text then
			self.headersMap.text = text
		end
		return GS.sendMessage( GS.forgeMessage( self.headersMap ))
	end
	
	if type(args) == "nil" or ( type(args) == "table" and args[0] < 1 ) then
		send( "NO ARGS WERE PASSED" )
		return
	end
	
	if args["--help"] then  -- Show help text
		send( self.HELPTEXT )
		return
	elseif args["--listfields"] then -- Show the available grouping fields
		send( self.GROUPBYFIELDS )
		return
	end
	
	local judge = (type(args["--judge"])=="string" and args["--judge"]:lower()) or
			(type(args["--oj"]) == "string" and args["--oj"]:lower())  or  nil
	local usernames = args["--by"]
	local startingTime = args["--since"] or ""
	local amount = args["--amount"] or string.format("%d", conf.DEFAULT_AMOUNT_SUBMISSIONS_SHOW)
	local tags = args["--tags"]
	local order = args["--order"]
	local verdicts = args["--verdicts"]
	local problemIds = args["--problems"]
	local submissionIds = args["--submissions"]
	local groupBy = args["--groupby"] -- by contigous time, by verdict, by tags, 
	local scheme = args["--show"]	-- To tweak the report scheme
	local timeIntervals = args["--timeintervals"] -- Intervals of time to filter
	local sendJSON = args["--json"]
	local fetch = args["--fetch"]
	
	
	-- ###########################################################################
	-- Check for some conditions about the arguments being passed
	-- USERNAMES
	if type(usernames) ~= "string" then
		send( "Missing or invalid usernames" )
		return nil
	
	-- JUDGE
	elseif type(judge) ~= "string" then
		send( "Missing or invalid judge name" )
		return nil
	elseif not Instances.JUDGES[judge] then
		send( "No such online judge" )
		return nil
	
	-- AMOUNT
	elseif type(amount) ~= "string" or util.tonumber(amount) < -1 or util.tonumber(amount) == 0 then
		send( "Invalid amount passed" )
		return nil
	
	-- STARTING TIME
	elseif type(startingTime) ~= "string" then
		send( "Invalid time passed" )
		return nil
	
	-- TAGS
	elseif tags and type(tags) ~= "string" then
		send( "Invalid tags datatype, expected string and got "..type(tags) )
		return nil
	
	-- VERDICTS
	elseif verdicts and type(verdicts) ~= "string" then
		send( "Invalid verdicts datatype, expected string and got "..type(verdicts) )
		return nil
	
	-- ORDER (SORTING)
	elseif order and type(order) ~= "string" then
		send( "Invalid order datatype, expected string and got "..type(order) )
		return nil
	elseif type(order) == "string" and not ("newest"):match("^"..order:lower()) and 
			not ("oldest"):match("^"..order:lower()) then
		send( "Invalid sorting manner passed, expected 'newest' or 'oldest', got " .. order )
		return nil
	
	-- SCHEME (REPORT)
	elseif scheme and type(scheme) ~= "string" then
		send( "Invalid report scheme (--show) datatype passed, expected string, got " .. type(scheme) )
		return nil
	
	-- PROBLEM IDs
	elseif problemIds and type("problemIds") ~= "string" then
		send( "Invalid list of problem IDs passed, expected a string, got " .. type(problemIds) )
		return nil
		
	-- SUBMISSION IDs
	elseif submissionIds and type("submissionIds") ~= "string" then
		send( "Invalid list of submission IDs passed, expected a string, got " .. type(submisionIds) )
		return nil
	
	-- TIME INTERVALS
	elseif timeIntervals and type(timeIntervals) ~= "string" then
		send( "Invalid intervals of time passed, expected a string, got " .. type(timeIntervals) )
		return nil
	
	-- TIME FILTERING CONFLICTS
	elseif args["--since"] and args["--timeintervals"] then
		send( "Please don't use both --since and --timeintervals at the same time. This may lead to incomplete reports." )
		return nil
	
	end
	-- ###########################################################################
	
	
	-- Set default values for the not passed arguments so all arguments can be
	-- passed forward without issues
	order = order and string.char(order:lower():byte(1)) == 'n' and 'newest' or order
	order = order and string.char(order:lower():byte(1)) == 'o' and 'oldest' or order
	
	amount = util.tonumber(amount) == -1 and math.pow(2,32) or util.tonumber(amount) 
	verdicts = verdicts and util.split(verdicts, '%s*,%s*')
	problemIds = problemIds and util.split(problemIds, '%s*,%s*')
	submissionIds = submissionIds and util.split(submissionIds, '%s*,%s*')
	tags = tags and util.split(tags, '%s*,%s*')
	usernames = usernames and util.split(usernames, '%s*,%s*')
	
	local multiplier, unit = startingTime:match( "^(%d+)(%a*)$" )
	multiplier = multiplier or string.format(os.time())
	unit = unit or 's'
	
	-- Handle the time argument like '4d' as 4 * Submissions.timeUnits['d']
	-- so it becomes 4 days in seconds
	startingTime = os.time() - util.tonumber(multiplier) * Submissions.timeUnits[unit:lower()]
	
	-- Get submissions from the respective judge requested by the user, pass all
	-- the arguments used for filtering to that judge too.
	local subs, status
	local update = false
	
	if fetch then
		subs, status = Instances.JUDGES[judge]:getSubmissions{
			usernames = usernames,
			startingTime = startingTime,
			tags = tags,
			verdicts = verdicts,
			problemIds = problemIds,
			submissionIds = submisionIds
		}
		update = true
	else
		-- Get submissions from the database
		
		local dbAccess = GS.newDatabaseAccess()
		subs, status = dbAccess:getSubmissions{
			usernames = usernames,
			startingTime = startingTime,
			tags = tags,
			verdicts = verdicts,
			problemIds = problemIds,
			submissionIds = submisionIds
		}
		dbAccess:close()
	end
	
	if status == "OK" then
		-- Store in databases
		if update then
			local dbAccess = GS.newDatabaseAccess()
			dbAccess:insertSubmissions( subs )
			dbAccess:close()
		end
		
		local quantity = math.min( #subs, amount )
		local reportScheme
		
		if type(scheme) == "string" then
			reportScheme = Submissions.createSubmissionReportStructure(scheme)
		end
		
		if order == "oldest" then
			table.sort( subs, Instances.JUDGES[judge].submissionNaturalComparator )
		elseif order == "newest" then
			table.sort( subs, Instances.JUDGES[judge].submissionReverseComparator )
		end
		
		if timeIntervals then
			local intervalsOfTime = util.split(timeIntervals, '%)(%s*,%s*)%(')
			subs = Submissions.grepByTimeIntervals( subs, intervalsOfTime )
			quantity = math.min( subs[0], quantity )
		end
		
		if groupBy then
			local groupingFields = util.split(groupBy, "%s*,%s*")
			for i = 1, #groupingFields do
				subs = Submissions.groupSubmissions( subs, groupingFields[i] )
			end
		end
		
		if quantity > 0 then
			if sendJSON then
				send( GS.objectToJson( subs ) )
			else
				send( 
					table.concat{
						(Submissions.makeReport(subs, quantity, reportScheme)),
						"The amount of shown submissions is: ",
						quantity
					}
				)
			end
		else
			send( "NO SUBMISSIONS TO SHOW." )
		end
	else
		send( "AN ERROR OCURRED - " .. (status or "") )
	end
	
	return 0
end

function Submissions.groupSubmissions( subs, field )
	if type(subs) ~= "table" then return {} end
	
	-- Check if field name is empty
	if util.trim(field):len() <= 0 then
		return subs, "Empty field name for grouping."
	end
	
	-- Check if there are submissions to group
	if (type(subs._amount) == "number" and subs._amount <= 0) and #subs <= 0 then
		return subs, "There are no submissions to group."
	end
	
	-- Check briefly if the field does exists
	if not subs._isGrouped_ and not util.getTableField(subs[1], field) then
		return subs, ("The field '" .. field .. "' does not exist.")
	end
	
	local groupedSubs = {}
	if subs._isGrouped_ then
		groupedSubs._groupedByField_ = subs._groupedByField_
		
		local quant = 0
		for index, value in pairs( subs ) do
			if type(value) == 'table' then
				groupedSubs[index] = Submissions.groupSubmissions(value, field)
				quant = quant + (groupedSubs[index]._amount or #groupedSubs[index])
			end
		end
		
		groupedSubs._amount = quant
	else
		local tmp
		
		for i = 1, #subs do			-- For each submission
			tmp = util.getTableField( subs[i], field)
			
			if tmp == nil then
				tmp = "No " .. field
			end
			
			if not groupedSubs[ tmp ] then
				groupedSubs[ tmp ] = {}
			end
			
			groupedSubs[ tmp ][ #groupedSubs[tmp] + 1 ] = subs[i]
		end
		
		groupedSubs._amount = #subs
		groupedSubs._groupedByField_ = field
	end
	
	groupedSubs._isGrouped_ = true
	return groupedSubs
	
	-- The grouped submissions are inside tables under numerical indexes, those
	-- tables are stored under non-numerical keys that are values (say numbers or
	-- strings) that the previous submissions do have. In english it is:
	--		GroupedSubmissions =
	--		{
	--			_isGrouped_ = true,
	--			_groupedByField_ = <Field>,
	--			[<Value>] = {
	--				<Some submissions that contains <Value> under it's field <Field>>
	--			},
	--			[<Another value>] = {
	--				<Other submissions that have <Another value> under <Field>>
	--			}
	--		}
	-- Now this layout gives the submissions a hierarchical structure that
	-- allows further grouping following the same concepts by grouping the "leaf"
	-- tables containing the submissions under numerical indexes and making them
	-- group those submissions under more tables using  their values for the <Field>
	-- as keys ... it's just growing down by a recursive method.
	
end

function Submissions.makeReport( submissions, remaining, scheme, nest_lvl )
	nest_lvl = type(nest_lvl) == 'number' and nest_lvl or 0
	
	if type(scheme) == 'nil' then
		scheme = Submissions.createSubmissionReportStructure(
			Submissions.defaultReportScheme
		)
	end
	
	local output = {scheme.headers, "\n"}
	local tabulation = ("  "):rep( nest_lvl )
	local processedSubmissions = 0
	
	if submissions._isGrouped_ then
		local answer, processed
		for key, val in pairs( submissions ) do

			if type(val) == 'table' then
				table.insert( output, tabulation )
				table.insert( output,	'[' )
				table.insert( output,	submissions._groupedByField_ )
				table.insert( output,	': ' )
				table.insert( output,	tostring(key) )
				table.insert( output,	']\n' )
				
				answer, processed = 
					Submissions.makeReport(val, remaining, scheme, nest_lvl + 1)
				
				table.insert( output,	answer )
				
				processedSubmissions = processedSubmissions + processed
				remaining = remaining - processed
				
				if remaining <= 0 then  break  end
			end
		end
	else
	
		for i = 1, math.min( #submissions, remaining ) do
			table.insert( output, tabulation )
			table.insert( output,	Submissions.describeSubmission( submissions[i], scheme ) )
			
			processedSubmissions = processedSubmissions + 1
		end
	end
	
	return table.concat(output), processedSubmissions
end

function Submissions.createSubmissionReportStructure( requiredFieldsToShow )
	local scheme = { headers = requiredFieldsToShow }
	local preffix, field, suffix
	local keepRaw, fieldName, maxLength
	
	while requiredFieldsToShow:len() > 0 do
		preffix, field, suffix = requiredFieldsToShow:match( "^([^']*)'?([^']*)'?(.*)$" )
		requiredFieldsToShow = suffix
		
		table.insert( scheme, preffix )
		
		keepRaw, fieldName, maxLength = util.trim(field):match( "^(%*?)([^:]+):?([0-9]*)$" )
		keepRaw = type(keepRaw) == "string" and string.len(keepRaw) > 0 and true
		
		if type(fieldName) == 'nil' or 
		   type(fieldName) == 'string' and string.len(fieldName) <= 0 then
			break
		end
		
		if type(maxLength) == 'string' then
			maxLength = util.tonumber(maxLength)
		else
			maxLength = nil
		end
		
		scheme[#scheme+1] = { 
			['field'] = fieldName, 
			['maxLength'] = maxLength,
			['method'] = keepRaw and Submissions.fieldMethods["_default"] or
				Submissions.fieldMethods[fieldName]
		}
	end
	
	return scheme
end

function Submissions.describeSubmission( submission, scheme )
	local description = {}
	
	if type(scheme) == 'nil' then
		scheme = Submissions.createSubmissionReportStructure(
			Submissions.defaultReportScheme
		)
	end
	
	local tmp
	for i = 1, #scheme do
		if type(scheme[i]) == 'string' then
			tmp = scheme[i]
		else
			tmp = util.getTableField( submission, scheme[i].field )
			
			if type(tmp) == 'table' then
				-- Just concats numeric values or strings that are put in a 
				-- 1-based array fashion, separated by ", "
				local first = true
				local str = {}
				for j = 1, #tmp do
					if type(tmp[j]) == 'number' or type(tmp[j]) == 'string' then
						if first then
							first = false
						else
							table.insert( str, ", " )
						end
						table.insert( str, tmp[j] )
					end
				end
				tmp = table.concat( str )
			else
				-- Apply the transformations being holded in scheme[i].method 
				tmp = scheme[i].method( tmp )
			end
			
			if type(scheme[i].maxLength) == 'number' then
				tmp = tmp:sub( 1, scheme[i].maxLength )
			end
		end
		
		table.insert( description, tmp )
	end
	
	table.insert( description, '\n' )
	return table.concat( description )
end

function Submissions.grepByTimeIntervals( subs, timeIntervals )
	
	if not timeIntervals or type(timeIntervals) ~= "table" then
		return subs, "No intervals of time were passed."
	end
	
	-- Check time intervals are valid
	for i in ipairs(timeIntervals) do
		if not timeIntervals[i]:match( "^%(.-,.-%)$" ) and not timeIntervals[i]:match( "^%(.-::.-%)$" ) then
			return subs, "Invalid interval of time: " .. timeIntervals[i]
		end
	end
	
	-- Since this does filter the submissions, the amount of submissions to show to
	-- the user may change before and after calling this function. To avoid problems
	-- by other code not knowing how many submissions there are we are storing the
	-- amount this function left in the index 0 of the resulting array. The amount
	-- is being accumulated in the variable quantity.
	local quantity, comments = 0, {}
	local timeLapse
	local intervals, filteredSubs = {}, {}
	local mult, unit, A, B
	
	-- Make an array of couple of times in seconds, so you can check if a submissions
	-- belongs to that time interval if it's inside that closed interval
	for i = 1, #timeIntervals do
		timeLapse = false
		-- Check if it's a comma separated interval
		A, B = timeIntervals[i]:match( "^%(%s*'?%s*(.-)%s*'?%s*,%s*'?%s*(.-)%s*'?%s*%)$" )
		if not A or not B then  -- Try with the double colon separated interval then
			A, B = timeIntervals[i]:match( "^%(%s*'?%s*(.-)%s*'?%s*::%s*'?%s*(.-)%s*'?%s*%)$" )
			timeLapse = true
		end
		
		-- Get the time in seconds for start and end of every time interval
		if A:match( "^%d+%a*$" ) then -- It's an amount of time relative to os.time(), meaning A time ago
			mult, unit = A:match( "^(%d+)(%a*)$" )
			mult = mult or "1"; 	unit = unit or 's';
			A = os.time() - util.tonumber(mult) * Submissions.timeUnits[unit:lower()]
		else  -- Get the time in seconds of the date that was passed using the LuaDate lib
		
			-- Check if the date passed is a valid one, that is MM/DD/YYYY [HH:MM[:SS]]
			local valid = true
			local d, t = A:match( "^%s*(%d%d?/%d%d?/%d%d%d%d)%s*(.-)%s*$" )
			
			if not d then valid = false
			elseif t and t:len() > 0 then
				d, t = t:match( "^%s*(%d%d?:%d%d?)%s*(.-)%s*$" )
				
				if not d then valid = false
				elseif t and t:len() > 0 then
					d = t:match( "^%s*:(%d%d?)%s*$" )
					
					if not d then valid = false end
				end
			end
			
			if valid then
				A = luadate.diff(luadate(A), luadate:epoch()):spanseconds()
			else
				table.insert( comments, "Invalid date passed: " )
				table.insert( comments, A )
				table.insert( comments, "\n" )
				A = nil
			end
		end
		
		if A and B:match( "^%d+%a*$" ) then 
			mult, unit = B:match( "^(%d+)(%a*)$" )
			mult = mult or "1"; 	unit = unit or 's';
			
			if timeLapse then -- It's an amount of time relative to A, meaning B time past A
				B = A + util.tonumber(mult) * Submissions.timeUnits[unit:lower()]
			else -- It's an amount of time relative to os.time(), meaning B time ago
				B = os.time() - util.tonumber(mult) * Submissions.timeUnits[unit:lower()]
			end
		elseif timeLapse then -- The second parameter must not be a date if describing a time lapse
			table.insert( comments, "Invalid interval of time: " )
			table.insert( comments, timeIntervals[i] )
			table.insert( comments, "\n" )
			B = nil
		else  -- Get the time in seconds of the date, like what was done with A
			
			-- Check if the date passed is a valid one, that is MM/DD/YYYY [HH:MM[:SS]]
			local valid = true
			local d, t = B:match( "^%s*(%d%d?/%d%d?/%d%d%d%d)%s*(.-)%s*$" )
			
			if not d then valid = false
			elseif t and t:len() > 0 then
				d, t = t:match( "^%s*(%d%d?:%d%d?)%s*(.-)%s*$" )
				
				if not d then valid = false
				elseif t and t:len() > 0 then
					d = t:match( "^%s*:(%d%d?)%s*$" )
					
					if not d then valid = false end
				end
			end
			
			if valid then
				B = luadate.diff(luadate(B), luadate:epoch()):spanseconds()
			else
				if A then -- If A is invalid then this one will be useless, but this may be valid besides that
					table.insert( comments, "Invalid date passed: " )
					table.insert( comments, B )
					table.insert( comments, "\n" )
				end
				B = nil
			end
		end
		
		if A and B then
			intervals[ #intervals + 1 ] = { A=A, B=B, desc=timeIntervals[i] }
		end
	end
	
	-- Get all the submissions belonging to every time interval
	local t, x
	for i = 1, #subs do
		for j = 1, #intervals do
			t = subs[i].creationTimeSeconds
			
			if t >= intervals[j].A and t <= intervals[j].B then
			  x = util.deepcopy( subs[i] )
			  x['timeInterval'] = intervals[j].desc
			  
			  filteredSubs[ #filteredSubs + 1 ] = x
			  quantity = quantity + 1
			end
			
		end
	end
	
	-- Group the submissions by the 'timeInterval' description, that is, the time 
	-- the user passed like "(4y::1d)" or "('07/22/2014','07/22/2014 12:00')"
	filteredSubs = Submissions.groupSubmissions( filteredSubs, 'timeInterval' )
	filteredSubs[0] = quantity
	return filteredSubs, table.concat( comments )
end

return Submissions
