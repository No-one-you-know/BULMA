local Compare = require('GeneralBehavior'):new()
local GS = require('GeneralStuff')

local Instances = Instances -- This avoids messing the global variable itself a bit

Compare.HELPTEXT = table.concat({
"COMPARE:  Compares some users by their submissions of each one.",
"    NOT IMPLEMENTED YET."
}, "\n")

function Compare:init( map )
	if type(map) == 'table' then
		self.headersMap = map
	end
end

function Compare:Run()
	local args = GS.getArguments( self.headersMap.text )
end

return Compare
