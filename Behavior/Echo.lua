local Echo = require('GeneralBehavior'):new()
local GS = require('GeneralStuff')

Echo.HELPTEXT = 
"ECHO:  Displays a line of text."

function Echo:init( map )
	if type(map) == 'table' then
		self.headersMap = map
	end
end

function Echo:Run()
	if not self.headersMap.text then
		self.headersMap.text = self.HELPTEXT
	end
	
	GS.sendMessage( GS.forgeMessage( self.headersMap ))
end

return Echo
