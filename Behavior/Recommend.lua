local Recommend = require('GeneralBehavior'):new()
local GS = require('GeneralStuff')

local Instances = Instances -- This avoids messing the global variable itself a bit

Recommend.HELPTEXT = table.concat({
"RECOMMEND:  Selects some problems based on popularity.",
"    NOT IMPLEMENTED YET."
}, "\n")

function Recommend:init( map )
	if type(map) == 'table' then
		self.headersMap = map
	end
end

function Recommend:Run()
	local args = GS.getArguments( self.headersMap.text )
end

return Recommend
