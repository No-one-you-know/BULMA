local GeneralBehavior = require('class'):new()

GeneralBehavior.headersMap = {}
GeneralBehavior.HELPTEXT = ""

-- Functions that must be called over some field values to change them, 
-- like taking a number meaning time in seconds and make it a human readable date
GeneralBehavior._secondsToDate = function(value) return os.date( "%D %H:%M:%S", value) end
GeneralBehavior._betterSizeInBytes = function(value)
	if value < 1e3 then return string.format("%.2f",value).."B" -- Bytes
	elseif value/1e3 < 1e3 then return string.format("%.2f",value/1e3).."KB" -- KiloBytes
	elseif value/(1e6) < 1e3 then return string.format("%.2f",value/1e6).."MB" -- MegaBytes
	elseif value/(1e9) < 1e3 then return string.format("%.2f",value/1e9).."GB" -- Gigabytes
	else return string.format("%.2f",value/1e12).."TB" -- TeraBytes, a bit of future proof :/
	end
end
GeneralBehavior._defaultTransform = function(value)
	if type(value) == "nil" then return "NO VALUE"
	elseif type(value) == "number" then return string.format("%d", value)
	else return value 
	end
end

function GeneralBehavior:Run()
	print "NOT IMPLEMENTED EXCEPTION"
end

return GeneralBehavior
