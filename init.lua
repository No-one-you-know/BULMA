
-- Add some paths for required libs and the whole source code
package.path = table.concat{
	package.path,
	";Libs/?.lua", ";Libs/?/init.lua", ";Libs/?/?.lua",
	";Misc/?.lua",
	";Behavior/?.lua",
	";Judges/?.lua",
	";DB/?.lua"
}

-- Loads all judges and commands and stores it in a global table called
-- 'Instances' so they are reachable by any module along the program
require('Instances')

return require("Bot")

--TODO: Problems.lua add the help and finish it.

--TODO: Submissions.lua add an alias for issuing the defaultReportScheme when using the --show arg so the user can use it plus other things, saving time and effort.
--TODO: Submissions.lua add a way to make sets of submissions that were created within a maximun difference of time, like "group of submissions created within 3 minutes between one and the next one" then you group submissions by that

--TODO: GeneralBehavior.lua Add a mechanism to show related info as errors or warnings to the user through a command's output, use comments variable for it and carry it across the whole command

--TODO: GeneralStuff.lua add a way to make the output of the commands go to private channels or direct messages to users if asked so, think about using the '!' character for that task in some part of the query

--TODO: Make the program to work without luajit too, find a library to work as a fallback for luajit-request.

--TODO: When the database is added, change the report schemes to be fetched from the database and let the user edit the default one
--TODO: Make the bot send results back to slack as snippets since way too much data is going to be sent. Make it optional too.

--##############################################################################

--DONE: DB.lua the function getProblems always returns "AN ERROR OCURRED" when called with the problems command
--DONE: DB.lua add a nickname filtering for the getSubmissions function

--DONE: Re-structure the HELPTEXT of all commands so they use table.concat and feature a more width-agnostic layout
--DONE: For the function comments, change the .. concatenation for table.concat
--DONE: Fix many string concatenations by using table.concat for performance and memory concerns
--DONE: Get a database to hold submissions previously fetched ... Postgres seems to be ok, sqlite was used tho

--DONE: Codeforces.lua Make the Codeforces:getProblems function
--DONE: Codeforces.lua Make faster the process of fetching submissions by fragmenting the requests

--DONE: Problems.lua add a normalizeProblem function and make it use the problemStatistics

--DONE: Submissions.lua show the amount of submissions being displayed as a footer to the submissions report.
--DONE: Submissions.lua make the time intervals processing more error proof, it's crashing a lot when using luadate. Check for the fields progressively ...
--DONE: Submissions.lua add some way to support intervals of time in the searches like from this day to this day and so on
--DONE: Submissions.lua add an argument to filter by problemId like it already filters by verdicts and tags
--DONE: Submissions.lua add an argument to show the available fields to use for the --groupBy functionality
--DONE: Submissions.lua think a way to simply and properly display the full name of the verdicts and some human readdable dates
--DONE: Submissions.lua add a header to the output, it's missing the column names. Use the scheme to do it.

--DONE: Import the commands and judges by using luafilesystem and massively requiring them, discard after the abstract class files
--DONE: Add a way to send the response as a serialized version of the submissions instead of text. (serialized to JSON)
--DONE: Change dkjson dependency and use lua-cjson since it's way faster, remember to change the README file after that.
--DONE: Fix some table.sort issues and add nickname filtering to DB.lua methods for returning submissions
