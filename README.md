# BULMA
This is a bot for tracking info of participants of competitions such as the ACM ICPC and many others, it can show the recent activities of a contestant in distincts online judges, show some stats about the ratio of AC/WA of the contestants and compare many of them. This bot is made using the Lua programming language.

# Dependencies
This bot runs over **luajit** and depends on many libraries for it to work, those being:

* [Lua-cjson](https://github.com/mpx/lua-cjson/)
* [LuaDate](https://github.com/Tieske/date)
* [luajit-request](https://github.com/LPGhatguy/luajit-request)
* [LuaFileSystem](https://github.com/keplerproject/luafilesystem)
* [Busted](https://github.com/Olivine-Labs/busted) (Optional: unit testing)

# Contact
If you got some questions about the bot, want to report/fix some bugs or want to discuss new features then you can drop at the IRC channel.

IRC: [#BULMADEV](irc://chat.oftc.net/BULMADEV "irc://chat.oftc.net/BULMADEV")
