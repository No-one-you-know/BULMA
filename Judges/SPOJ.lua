local SPOJ = require("GeneralJudge"):new()

SPOJ.NAME = "Sphere Online Judge (SPOJ)"
SPOJ.URL = "spoj.com"
SPOJ.QUERYURL = ""

function SPOJ:new()
	return SPOJ
end

return SPOJ
