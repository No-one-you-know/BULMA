local CodeForces = require('GeneralJudge'):new()
local GS = require('GeneralStuff')

local socket = require 'socket'

local Instances = Instances -- This avoids messing the global variable itself a bit

CodeForces.NAME = "CodeForces"
CodeForces.URL = "https://codeforces.com/"
CodeForces.QUERYURL = CodeForces.URL .. "api/"
CodeForces.PROBLEMSURL = CodeForces.URL .. "problemset/problem/"

function CodeForces.new()
	return CodeForces
end

CodeForces.verdictEnum = 
{
	["OK"] =                    CodeForces:normalizeVerdict("AC"),
	["WRONG_ANSWER"] =          CodeForces:normalizeVerdict("WA"),
	['COMPILATION_ERROR'] =     CodeForces:normalizeVerdict("CE"),
	['PRESENTATION_ERROR'] =    CodeForces:normalizeVerdict("PE"),
	['RUNTIME_ERROR'] =         CodeForces:normalizeVerdict("RE"),
	['TIME_LIMIT_EXCEEDED'] =   CodeForces:normalizeVerdict("TLE"),
	['MEMORY_LIMIT_EXCEEDED'] = CodeForces:normalizeVerdict("MLE"),
	['CHALLENGED'] =            CodeForces:normalizeVerdict("HACKED")
}

function CodeForces:getProblems( arguments )
	local selfname = "CodeForces.getProblems"
	local comments = {}
	
	if not arguments then  return nil, selfname..":No arguments were passed.\n"  end
	
	local tags = arguments.tags
	
	-- Get problems from CodeForces.com
	local probs, stats, status, response
	local queryUrl = {self.QUERYURL, "problemset.problems"}
	
	if tags then
		table.insert( queryUrl, "?tags=" )
		for i = 1, #tags do
			table.insert( queryUrl, tags[i] )
			table.insert( queryUrl, ";" )
		end
		queryUrl[#queryUrl] = nil -- Remove the last semicolon
	end
	queryUrl = table.concat( queryUrl )
	
	response = GS.getResponseObject( queryUrl )
	while response.status == "FAILED" and response.comment == "Call limit exceeded" do
		-- Sleeps for 400 ms before retrying. Codeforces blocks connections for 
		-- 1 second when too many connections are being made, so we will wait at 
		-- most twice per second (with an ideal network connection).
		socket.sleep( 0.400 )
		response = GS.getResponseObject( queryUrl )
	end

	status = response.status
	
	if status ~= "OK" then
		return nil, 
			table.concat{ status, "\n", (response.comment or "") }, 
			table.concat( comments )
	end

	-- response.result has 2 things: 
	--   a list of problems and a list of statistics for those problems
	probs, stats = response.result.problems, response.result.problemStatistics
	
	probs = self:filterProblems( probs, arguments )
	return probs, status, table.concat( comments )
end

function CodeForces:getSubmissions( arguments )
	local selfname = "CodeForces.getSubmissions"
	local comments = {}
	
	local usernames    = arguments.usernames
	local startingTime = arguments.startingTime
	local tags         = arguments.tags
	local verdicts     = arguments.verdicts
	
	if type(usernames) ~= "table" then
		table.insert( comments, selfname )
		table.insert( comments, ":USERNAME LIST PASSED IS NOT A TABLE.\n" )
		return nil, table.concat( comments )
	end
	
	if tags and type(tags) ~= "table" then
		table.insert( comments, selfname )
		table.insert( comments, ":TAG(S) PASSED IS NOT A TABLE.\n" )
		return nil, table.concat( comments )
	end
	
	if verdicts and type(verdicts) ~= "table" then
		table.insert( comments, selfname )
		table.insert( comments, ":VERDICT(S) PASSED IS NOT A TABLE.\n" )
		return nil, table.concat( comments )
	end
	
	local subs, status, response = {}, {}
	local queryUrl = {self.QUERYURL, "user.status", "?handle="}
	
	for i = 1, #usernames do
		table.insert( queryUrl, usernames[i] )
		-- Batched fetching only makes sense with queries limitted by time
		if startingTime then
			
			local bufferStep, offset = 1000, 1
			table.insert( queryUrl, "&count=" )
			table.insert( queryUrl, bufferStep )
			
			while true do
				table.insert( queryUrl, "&from=" )
				table.insert( queryUrl, offset )
				response = GS.getResponseObject( table.concat(queryUrl) )
				queryUrl[#queryUrl] = nil
				queryUrl[#queryUrl] = nil
				
				if type(response) ~= "table" or response.status ~= "OK" then
					table.insert( comments, selfname )
					table.insert( comments, ":SOMETHING WENT WRONG WITH THE HTTP QUERY.\n" )
					if type(response) == "table" and type(response.comment) == "string" then
						table.insert( comments, "The " )
						table.insert( comments, self.NAME )
						table.insert( comments, " site said: " )
						table.insert( comments, response.comment )
						table.insert( comments, "\n" )
					end
					return nil, table.concat( comments )
				end
				
				while response.status == "FAILED" and 
					response.comment == "Call limit exceeded" do
					
					-- Sleeps for 400 ms before retrying. Codeforces blocks connections for 
					-- 1 second when too many connections are being made, so we will wait at 
					-- most twice per second (with an ideal network connection).
					socket.sleep( 0.400 )
					
					table.insert( queryUrl, "&from=" )
					table.insert( queryUrl, offset )
					response = GS.getResponseObject( table.concat(queryUrl) )
					queryUrl[#queryUrl] = nil
					queryUrl[#queryUrl] = nil
				end
				
				status = response.status
				
				if status ~= "OK" then
					return nil, 
						table.concat{ status, "\n", (response.comment or "") }, 
						table.concat( comments )
				end
				
				for i = 1, #response.result do
					subs[ #subs + 1 ] = response.result[i]
				end
				
				-- If received less submissions than the asked amount, then we got all of them
				if #response.result < bufferStep then  
					break
				-- If the last submission received is older than the startingTime passed
				elseif subs[ #subs ].creationTimeSeconds < startingTime then  
					break  
				end
				
				-- Set the id for the next expected submission to be received
				offset = offset + bufferStep
			end
			
			queryUrl[#queryUrl] = nil
			queryUrl[#queryUrl] = nil
		else
			-- If not filtering with the --since argument, then just get the whole
			-- list of submissions, it's likely to be faster this cases.
			response = GS.getResponseObject( queryUrl )
			
			subs = response.result
			status = response.status
		end
		
		queryUrl[#queryUrl] = nil
	end
	
	arguments.usernames = nil -- It's not necessary to filter by usernames
	subs = self:filterSubmissions( subs, arguments )
	return subs, status, table.concat( comments )
end

function CodeForces:filterSubmissions( submissions, arguments )
	
	-- Normalize before anything
	for i = 1, #submissions do
		self.normalizeSubmission( submissions[i] )
	end
	
	-- Filter submissions
	self:filterSubmissionsByTime(     submissions, arguments.startingTime )
	self:filterSubmissionsByNickname( submissions, arguments.usernames )
	self:filterSubmissionsByTag(      submissions, arguments.tags )
	self:filterSubmissionsByVerdict(  submissions, arguments.verdicts )
	self:filterSubmissionsByProblem(  submissions, arguments.problemIds )
	self:filterSubmissionsById(       submissions, arguments.submissionsId )
	
	-- Sort them from newest to oldest
	table.sort(submissions, CodeForces.submissionReverseComparator)
	return submissions
end

function CodeForces:filterProblems( problems, arguments )
	
	-- Normalize before anything
	for i = 1, #problems do
		self.normalizeProblem( problems[i] )
	end
	
	-- Filter problems
	self:filterProblemsByTag( problems, arguments.tags )
	self:filterProblemsById(  problems, arguments.problemIds )
	
	-- Sort them from newest to oldest
	table.sort(problems, CodeForces.problemReverseComparator)
	return problems
end

function CodeForces.normalizeSubmission( submission )
	-- Selecting the respective verdict from GeneralJudge instead of the 
	-- codeforces particular ones
	submission.verdict = 
		CodeForces.verdictEnum[ submission.verdict ] or submission.verdict
	
	-- Adding a field with the full name of the verdict
	submission.fullVerdictName = 
		CodeForces.fullNameVerdictEnum[ submission.verdict ] or 
			submission.fullNameVerdictEnum
	
	-- Adding a link to the page of this submission's problem
	submission.problem.link = table.concat{
		CodeForces.PROBLEMSURL, 
		submission.problem.contestId, 
		'/',
		submission.problem.index
	}
	
	submission.problem.judge = CodeForces.NAME:lower()
	submission.judge = CodeForces.NAME:lower()
	
	return submission
end

function CodeForces.normalizeProblem( problem, statistics )
	-- Add the count of people that have solved this problem into the problem structure
	-- so it can be showed.
	if statistics then
		problem.solvedCount = statistics.solvedCount
	end
	-- Add a link leading to the problem's webpage
	problem.link = table.concat{
		CodeForces.PROBLEMSURL,
		problem.contestId,
		"/",
		problem.index
	}
	
	problem.judge = CodeForces.NAME:lower()
	
	return problem
end

return CodeForces
