local UVA = require("GeneralJudge"):new()

UVA.NAME = "UVa Online Judge"
UVA.URL = "uva.onlinejudge.org"
UVA.QUERYURL = ""

function UVA:new()
	return UVA
end

return UVA
