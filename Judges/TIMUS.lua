local TIMUS = require("GeneralJudge"):new()

TIMUS.NAME = "Timus Online Judge"
TIMUS.URL = "acm.timus.ru"
TIMUS.QUERYURL = ""

function TIMUS:new()
	return TIMUS
end

return TIMUS
