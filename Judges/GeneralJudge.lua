local GeneralJudge = require('class'):new()
local util = require('Utilities')

local Instances = Instances -- This avoids messing the global variable itself a bit

GeneralJudge.NAME = ""
GeneralJudge.URL = ""
GeneralJudge.QUERYURL = ""

-- The judges do reference to here to get these the general names of the verdicts
-- for the user input and sorting/filtering/grouping stuff
GeneralJudge.verdictEnum =
{
	["AC"]     = "AC",
	["WA"]     = "WA",
	["CE"]     = "CE",
	["PE"]     = "PE",
	["RE"]     = "RE",
	["TLE"]    = "TLE",
	["MLE"]    = "MLE",
	["HACKED"] = "HACKED"
}

-- This are the full names of the verdicts above, used in the fullVerdictName field
-- of each submission after being normalized.
GeneralJudge.fullNameVerdictEnum =
{
	["AC"]     = "ACCEPTED",
	["WA"]     = "WRONG ANSWER",
	["CE"]     = "COMPILATION ERROR",
	["PE"]     = "PRESENTATION ERROR",
	["RE"]     = "RUNTIME ERROR",
	["TLE"]    = "TIME LIMIT EXCEEDED",
	["MLE"]    = "MEMORY LIMIT EXCEEDED",
	["HACKED"] = "HACKED"
}

-- Comparators for sorting the submissions by creation time
GeneralJudge.submissionNaturalComparator = 	-- Natural comparator
function( sub1, sub2 )
	return (sub1.creationTimeSeconds < sub2.creationTimeSeconds) and true or false
end
GeneralJudge.submissionsReverseComparator = 	-- Reverse comparator
function( sub1, sub2 )
	return (sub1.creationTimeSeconds > sub2.creationTimeSeconds) and true or false
end

-- Comparators for sorting the problems by contestId and index
GeneralJudge.problemNaturalComparator = 	-- Natural comparator
function( prob1, prob2 )
	if prob1.contestId < prob2.contestId then
		return true
	elseif prob1.contestId == prob2.contestId then
		return (prob1.index < prob2.index) and true or false
	else
		return false
	end
end
GeneralJudge.problemReverseComparator = 	-- Reverse comparator
function( prob1, prob2 )
	if prob1.contestId > prob2.contestId then
		return true
	elseif prob1.contestId == prob2.contestId then
		return (prob1.index > prob2.index) and true or false
	else
		return false
	end
end

function GeneralJudge:getSubmissions( arguments )
	print "NOT IMPLEMENTED EXCEPTION"
end

function GeneralJudge:getProblems( arguments )
	print "NOT IMPLEMENTED EXCEPTION"
end

function GeneralJudge:normalizeVerdict( verdict )
	if type(verdict) == "string" then
		return self.__baseclass.verdictEnum[ verdict:upper() ]
	end
end

function GeneralJudge:normalizeSubmission( submission )
	return submission
end

function GeneralJudge:normalizeProblem( problem )
	return problem
end

-- NEXT FUNCTIONS DO CHANGE THE PASSED TABLE LIKE SECCOND EFFECTS IN FUNCTIONAL 
-- PROGRAMMING, KEEP IT IN MIND
function GeneralJudge:filterSubmissionsByTag( submissions, expectedTags )
	if not expectedTags then return submissions, "No list of tags passed.\n" end
	
	for i = #submissions, 1, -1 do
		-- Remove the ones that do not contain at least all the tags from 
		-- the passed tags array
		if not util.containsAll( submissions[i].problem.tags, expectedTags ) then
			-- Swap with the last one and remove it
			submissions[i] = submissions[ #submissions ]
			submissions[ #submissions ] = nil
		end
	end
	
	return submissions
end

function GeneralJudge:filterSubmissionsByVerdict( submissions, expectedVerdicts )
	if not expectedVerdicts then return submissions, "No list of verdicts passed.\n" end
	
	for i = #submissions, 1, -1 do
		-- Remove the ones that do not contain a single verdict from the
		-- passed verdict array
		
		if not util.containsAny(self:normalizeVerdict(submissions[i].verdict), 
				expectedVerdicts ) then
			-- Swap with the last one and remove it
			submissions[i] = submissions[ #submissions ]
			submissions[ #submissions ] = nil
		end
	end
	
	return submissions
end

function GeneralJudge:filterSubmissionsByTime( submissions, startingTime )
	
	if not startingTime then  startingTime = 0
	else  startingTime = math.max(0, startingTime) end
	
	if startingTime >= os.time() then  return {}, "Invalid time passed.\n"  end
	
	-- Sort them by time, from oldest to newest
	table.sort( submissions, self.naturalComparator )
	
	local firstIndex, lastIndex
	
	-- Indexes that limit the submissions inside the time interval
	firstIndex = util.lowerBound( submissions, {creationTimeSeconds = startingTime}, 
		self.naturalComparator )
	lastIndex  = #submissions
	
	-- Overwrite the first elements with the one that lay inside the valid time interval
	for i = firstIndex, lastIndex do
		submissions[i - firstIndex + 1] = submissions[i]
	end
	
	-- Wipe the remaining elements of the table
	for i = lastIndex-firstIndex + 2, #submissions do
		submissions[i] = nil
	end
	
	return submissions
end

function GeneralJudge:filterSubmissionsByProblem( submissions, problemIds )
	-- problemIds is a table containing problemIDs
	if not problemIds or type(problemIds) ~= "table" then 
		return submissions, "Invalid list of problemIDs.\n"
	end
	
	if #problemIds < 1 then  return submissions, "No problem IDs were passed.\n"  end
	
	local problemID = {}
	
	for i = #submissions, 1, -1 do
		problemID[1] = submissions[i].problem.contestId .. submissions[i].problem.index
		
		if not util.containsAny( problemID, problemIds) then
			submissions[ i ] = submissions[ #submissions ]
			submissions[ #submissions ] = nil
		end
	end
	
	return submissions
end

function GeneralJudge:filterSubmissionsByNickname( submissions, nicknames )
	
	if type(nicknames) == "string" then
		nicknames = {nicknames}
	elseif type(nicknames) ~= "table" then 
		return submissions, "Invalid list of nicknames passed.\n"
	end
	
	if #nicknames < 1 then  return submissions, "No nicknames were passed.\n"  end
	
	local nicks = {}
	
	for i = #submissions, 1, -1 do
		for k = 1, #nicks do
			nicks[k] = nil
		end
		
		-- Fill the nicks table with the authors' usernames of the i-th submission
		for j = 1, #submissions[i].author.members do
			nicks[j] = submissions[i].author.members[j].handle
		end
		
		if not util.containsAny( nicks, nicknames ) then
			submissions[ i ] = submissions[ #submissions ]
			submissions[ #submissions ] = nil
		end
	end
	
	return submissions
end

function GeneralJudge:filterSubmissionsById( submissions, submissionIds )
	-- problemIds is a table containing problemIDs
	if not submissionIds or type(submissionIds) ~= "table" then 
		return submissions, "Invalid list of submissionIDs.\n"
	end
	
	if #submissionIds < 1 then  return submissions, "No submission IDs were passed.\n"  end
	
	local submissionID = {}
	
	for i = #submissions, 1, -1 do
		submissionID[1] = submissions[i].contestId .. submissions[i].index
		
		if not util.containsAny( submissionID, submissionIds) then
			submissions[ i ] = submissions[ #submissions ]
			submissions[ #submissions ] = nil
		end
	end
	
	return submissions
end

function GeneralJudge:filterSubmissions( submissions, arguments )
	return submissions
end

function GeneralJudge:filterProblemsByTag( problems, expectedTags )
	if not expectedTags then return problems, "No list of tags passed.\n" end
	
	for i = #problems, 1, -1 do
		-- Remove the ones that do not contain at least all the tags from 
		-- the passed tags array
		if not util.containsAll( problems[i].tags, expectedTags ) then
			-- Swap with the last one and remove it
			problems[i] = problems[ #problems ]
			problems[ #problems ] = nil
		end
	end
	
	return problems
end

function GeneralJudge:filterProblemsById( problems, problemIds )
		-- problemIds is a table containing problemIDs
	if not problemIds or type(problemIds) ~= "table" then 
		return problems, "Invalid list of problemIDs, returning all problems.\n"
	end
	
	if #problemIds < 1 then  return problems, "No problem IDs were passed.\n"  end
	
	local problemID = {}
	
	for i = #problems, 1, -1 do
		problemID[1] = problems[i].contestId .. problems[i].index
		
		if not util.containsAny( problemID, problemIds) then
			problems[ i ] = problems[ #problems ]
			problems[ #problems ] = nil
		end
	end
	
	return problems
end

function GeneralJudge:filterProblems( problems, arguments )
	return problems
end

return GeneralJudge

--[[
	GeneralJudge:normalizeSubmission is a function that will be defined by each
	judge class (codeforces, spoj, coj, etc ...) so every judge can transform
	their submissions structure into a common structure that is easier to work.
	
	The preferred structure for the submissions are the ones defined by the 
	codeforces judge and the other judges will transform their submissions to
	play nice with this structure as much as possible.
]]
