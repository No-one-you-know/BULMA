context( "GeneralStuff.lua - ", 
	function()
		
		setup(
			function()
				old_package_path = package.path
			
				package.path = package.path ..
					";../Libs/?.lua" .. ";../Libs/?/init.lua" .. ";../Libs/?/?.lua" ..
					";../Misc/?.lua" .. ";../Behavior/?.lua" .. ";../Judges/?.lua"
				
				require "GeneralStuff"
			end
		)
		
		teardown(
			function()
				package.path = old_package_path
			end
		)
		
		
		test( "getArguments", 
			function()
				sample = "elephant --height 18 --width 7 --s M --c B --alive --age 50"
				assert.are.same( GeneralStuff.getArguments(sample), 
					{[0] = 7, ["--height"] = "18", ["--width"] = "7",
					["--s"] = "M", ["--c"] = "B", ["--alive"] = true, ["--age"] = "50"})
			end
		)
		
	end
)
