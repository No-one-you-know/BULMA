context( "Utilities.lua -", 
	function()

		setup(
			function()
				old_package_path = package.path
			
				package.path = package.path ..
					";../Libs/?.lua" .. ";../Libs/?/init.lua" .. ";../Libs/?/?.lua" ..
					";../Misc/?.lua" .. ";../Behavior/?.lua" .. ";../Judges/?.lua"
				
				util = require "Utilities"
			end
		)
		
		teardown(
			function()
				package.path = old_package_path
			end
		)
		
		-- String tests
		test( "string.trim",
			function()
				sample = "  Hello World !    "
				assert.are.equal( util.trim(sample), "Hello World !" )
			end
		)
		test( "string.split",
			function()
				sample = "a bc def 12345"
				assert.are.same( util.split(sample, " "), {"a","bc","def","12345"} )
				assert.are.same( util.split(sample, "def"), {"a bc "," 12345"} )
				assert.are.same( util.split(sample, "a"), {" bc def 12345"} )
				assert.are.same( util.split(sample, "13"), {"a bc def 12345"} )
				assert.are.same( util.split(sample, ""), {"a bc def 12345"} )
				assert.are.same( util.split(sample), {"a bc def 12345"} )
			end
		)
		test( "string.tonumber",
			function()
				sample = "1234567890"
				assert.are.equal( util.tonumber(sample), 1234567890 )
			end
		)
		test( "string.tonumber",
			function()
				sample = "1234567890.472"
				assert.is.equal( util.tonumber(sample), 1234567890.472 )
			end
		)
		test( "string.tonumber",
			function()
				sample = "-1890"
				assert.is.equal( util.tonumber(sample), -1890 )
			end
		)
		test( "string.tonumber",
			function()
				sample = "-10.22"
				assert.are.equal( util.tonumber(sample), -10.22 )
			end
		)
		test( "string.tonumber",
			function()
				sample = ".72"
				assert.are.equal( util.tonumber(sample), 0.72 )
			end
		)
		
		-- ContainsAll and ContainsAny tests
		test( "containsAll",
			function()
				sample = {1,7,"Aloh!",math.random, -1.2, 2, function() print "hi" end}
				assert.is_true( util.containsAll( sample, {-1.2,} ) )
				assert.is_true( util.containsAll( sample, {"Aloh!",7,math.random,1} ) )
				assert.is_true( util.containsAll( sample, {7,math.random,1} ) )
				assert.is_true( util.containsAll( sample, {1} ) )
				assert.is_true( util.containsAll( sample, {} ) )
			end
		)
		test( "containsAny",
			function()
				sample = {0,2,4,6,8,10,12,14,16,18,20}
				assert.is_true( util.containsAny( sample, {3,6,9,12,15,18,21,24,27,30} ) )
				assert.is_true( util.containsAny( sample, {1,2,3,4,5} ) )
				assert.is_true( util.containsAny( sample, {0,100,200,300,400,500} ) )
				assert.is_true( util.containsAny( sample, {20,18,16,14,12} ) )
			end
		)
		
		-- table.getField tests
		test( "table.getfield",
			function()
				sample = {first = {second = {third = {fourth = {fifth = true}}}}, x=4}
				assert.is_true( util.getTableField(sample,"first.second.third.fourth.fifth") )
				assert.is_truthy( util.getTableField(sample,"x") )
			end
		)
		
	end
)
