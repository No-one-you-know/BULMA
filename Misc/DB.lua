local DB = require('class'):new()
local util = require('Utilities')
local instances = Instances

function DB:init(config)
	
	if config then
		self.DATABASE_ENGINE        = config.DATABASE_ENGINE
		self.DATABASE_FILE_LOCATION = config.DATABASE_FILE_LOCATION or ""
		self.DATABASE_NAME          = config.DATABASE_NAME
	else
		self.DATABASE_ENGINE        = "SQLITE3"
		self.DATABASE_FILE_LOCATION = ""
		self.DATABASE_NAME          = "BULMA"
	end
	
	local luasql = require('luasql.'..self.DATABASE_ENGINE:lower())
	self._environment = luasql[self.DATABASE_ENGINE:lower()]()
	
	if self.DATABASE_ENGINE:lower():match("sqlite.*") then
		self.DATABASE_NAME = self.DATABASE_FILE_LOCATION .. self.DATABASE_NAME
		
		if not self.DATABASE_NAME:match(".*(%.db)$") then
			self.DATABASE_NAME = self.DATABASE_NAME .. ".db"
		end
	end
	
	self._connection = self._environment:connect( self.DATABASE_NAME )
end

function DB:close()
	self._connection:close()
end

function DB:prepareProblemForStoring(problem)
	problem.tags = table.concat( problem.tags, ", ")
	return problem
end

function DB:prepareProblemForUse(problem)
	problem.tags = util.split( problem.tags, "%s*,%s*" )
	return problem
end

function DB:prepareSubmissionForStoring(submission)
	submission.problem_contestId = submission.problem.contestId
	submission.problem_index = submission.problem.index
	
	local t = {}
	for i = 1, #submission.author.members do
		t[i] = submission.author.members[i].handle
	end
	
	submission.author_members = table.concat( t , ", ")
	submission.author_contestId = submission.author.contestId
	
	return submission
end

function DB:prepareSubmissionForUse(submission)
	
	local values = self:getProblems{ 
		contestId = submission.problem_contestId, 
		index = submission.problem_index, 
		judge = submission.judge
	}
	submission.problem = values[1]
	
	submission.problem_contestId = nil
	submission.problem_index = nil
	
	values = util.split( submission.author_members, "%s*,%s*" )
	submission.author = {
		contestId = submission.author_contestId,
		members = {}
	}
	for i = 1, #values do
		submission.author.members[i] = { handle = values[i] }
	end
	
	submission.author_contestId = nil
	submission.author_members = nil
	
	return submission
end

function DB:getProblems(args)
	local query = {"SELECT * FROM Problems "}
	local probs, status = {}, "OK"
	local flag = false
	local params = {}
	
	if args then
		params.judge     = args.judge
		params.contestId = args.contestId
		params.index     = args.index
	end
	
	for key, value in pairs( params ) do
			table.insert( query, (flag and "and " or "where ") )
			table.insert( query, "`" )
			table.insert( query, key )
			table.insert( query, "`" )
			table.insert( query, " = '")
			table.insert( query, value or "")
			table.insert( query, "' ")
			flag = true
		end
	
	table.insert( query, ";" )
	query = table.concat( query )
	
	local cursor = self._connection:execute( query )
	local row, err = cursor:fetch( {}, "a" )
	
	if type(row) ~= "table" then
		status = row or status
		return {}, status
	end
	
	while row do
		table.insert( probs, self:prepareProblemForUse( row ) )
		row = cursor:fetch( {}, "a" )
	end
	cursor:close()
	
	local t, filteredProbs = {}, {}
	
	-- Group problems by their judges
	for i = 1, #probs do
		if not t[ probs[i].judge ] then 
			t[ probs[i].judge ] = {}
		end
		table.insert( t[ probs[i].judge ], probs[i])
	end
	
	-- Filter the problems of each judge and make one single list 
	-- with the remaining problems
	for key, value in pairs(t) do
		t[key] = instances.JUDGES[ key ]:filterProblems( value, args )
		for i = 1, #t[key] do
			table.insert( filteredProbs, t[key][i] )
		end
	end
	
	return filteredProbs, status
end

function DB:getSubmissions(args)
	local query = {"SELECT * FROM Submissions "}
	local subs, status = {}, "OK"
	
	table.insert( query, ";" )
	query = table.concat( query )
	
	local cursor, row = self._connection:execute( query )
	if not cursor then
		status = "FAILED TO ISSUE SQL QUERY TO DATABASE."
	else
		row = cursor:fetch({}, "a")
	end
	
	if type(row) ~= "table" then
		status = row
		return {}, status
	end
	
	while row do
		table.insert( subs, self:prepareSubmissionForUse( row ) )
		row = cursor:fetch({}, "a")
	end
	cursor:close()
	
	local t, filteredSubs = {}, {}
	
	-- Group submissions by their judges
	for i = 1, #subs do
		if not t[ subs[i].judge ] then 
			t[ subs[i].judge ] = {}
		end
		table.insert( t[ subs[i].judge ], subs[i])
	end
	
	-- Filter the submissions of each judge and make one single list 
	-- with the remaining submissions
	for key, value in pairs(t) do
		t[key] = instances.JUDGES[ key ]:filterSubmissions( value, args )
		for i = 1, #t[key] do
			table.insert( filteredSubs, t[key][i] )
		end
	end
	
	return filteredSubs, status
end

function DB:insertProblems(problems)
	
	-- query: table for concatenating distinct values into one whole query
	-- tmp_query: table for surrounding values around ""
	
	local query = {"INSERT OR REPLACE INTO Problems VALUES( ", "", ");"}
	local tmp, tmp_query = {}, {'"', '', '"'}
	local cursor = self._connection:execute("SELECT * FROM Problems;")
	local cols = cursor:getcolnames()
	cursor:close()
	
	-- Turn off autocommit for a transaction alike insert batch
	self._connection:setautocommit( false, "IMMEDIATE" )
	self._connection:execute( "BEGIN TRANSACTION;" )
	
	for i = 1, #problems do
		problems[i] = self:prepareProblemForStoring( problems[i] )
		for j = 1, #cols do
			tmp_query[2] = problems[ i ][ cols[j] ] or ""
			tmp[ j ] = table.concat( tmp_query )
		end
		query[ 2 ] = table.concat( tmp, "," )
		
		
		self._connection:execute( table.concat( query ) )
	end
	
	self._connection:execute( "COMMIT;" )
	
	-- Commits and turns on the autocommit
	self._connection:commit()
	return true
end

function DB:insertSubmissions(submissions)
  
  if not submissions then return false end
  
	local tmp, tmp_query = {}, {'"', '', '"'}
	
	for i = 1, #submissions do
		tmp[i] = submissions[i].problem
	end
	self:insertProblems( tmp )
	tmp = {}
	
	local query = {"INSERT OR REPLACE INTO Submissions VALUES( ", "", "); "}
	local cursor = self._connection:execute("SELECT * FROM Submissions;")
	local cols = cursor:getcolnames()
	cursor:close()
	
	-- Turn off autocommit for batching inserts
	self._connection:setautocommit( false, "IMMEDIATE" )
	self._connection:execute( "BEGIN TRANSACTION;" )
	
	for i = 1, #submissions do
		submissions[i] = self:prepareSubmissionForStoring( submissions[i] )
		for j = 1, #cols do
			tmp_query[2] = submissions[ i ][ cols[j] ]
			tmp[ j ] = table.concat( tmp_query )
		end
		query[ 2 ] = table.concat( tmp, "," )
		
		self._connection:execute( table.concat( query ) )
	end
	
	self._connection:execute( "COMMIT;" )
	
	-- Commits and turns on the autocommit
	self._connection:commit()
	return true
end

return DB

-- DONE: (Uses the OOP class.lua) Needs a function called DB.new that will return a database access object with methods to insert/get stuff back and forth the database.
-- DONE: Needs a function to close a database connection cleanly.
-- DONE: Needs methods to convert the actual NormalizedSubmissions structure into the database submissions model and viceversa.
