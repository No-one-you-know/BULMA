local utilities = {}

utilities.timeUnits = -- This tables holds the amount of seconds for a time unit
{
	['']   = 1, -- by default use seconds as the time unit
	['s']  = 1,                  -- 1 second
	['mi'] = 60,                 -- 1 minute = 60 seconds
	['h']  = 60 * 60,            -- 1 hour   = 60 minutes
	['d']  = 60 * 60 * 24,       -- 1 day    = 24 hours
	['w']  = 60 * 60 * 24 * 7,   -- 1 week   = 7 days
	['m']  = 60 * 60 * 24 * 30,  -- 1 month  = 30 days (for our purposes)
	['y']  = 60 * 60 * 24 * 365, -- 1 year   = 365 days
}

function utilities.trim(str)
	return str:match("[ |\t]*(.-)[ |\t]*$")
end

function utilities.split(str, pattern)
	local sections = {}
	local i, j
	
	if type(pattern) ~= "string" or string.len(pattern) <= 0 then
		return {str}
	end
	
	for	substr in str:gmatch( pattern ) do
		i, j = str:find( substr, 1, true ) -- Pattern matching disabled
		
		if i > 1 then -- If there's something before the pattern, add it
			sections[ #sections + 1 ] = str:sub(1, i-1)
		end
		
		-- The str now starts right after the last ocurrence of the pattern
		str = str:sub(j+1)
	end
	
	-- If the rest of the str is not empty, then add it
	if str:len() > 0 then
		sections[ #sections + 1] = str
	end
	
	return sections
end

function utilities.tonumber( str )
	local sign = str:match("^[ |\t]*(-?)[ |\t]*")
	local val  = str:match("^[ |\t]*-?([0-9]+)[ |\t]*")
	local dec  = str:match("[ |\t]*(%.[0-9]+)[ |\t]*$")
	if val == nil and dec == nil then return nil end
	sign = sign == '-' and -1 or 1
	val = val ~= nil and (val+0)*sign or 0
	dec = dec ~= nil and (dec+0)*sign or 0
	return (val ~= nil) and val+0 + dec
end

function utilities.lowerBound( tab, val, cmp, low, hi)
	if type(tab) ~= "table" then
		return nil, "TABLE EXPECTED"
	elseif #tab <= 0 then
		return 0, tab[0]
	end
	
	if type(low) == "string" then
		low = low:tonumber()
	elseif type(low) == "number" then
		low = math.abs( low )
	else
		low = 1
	end
	
	if type(hi) == "string" then
		hi = hi:tonumber()
	elseif type(hi) == "number" then
		hi = math.abs( hi )
	else
		hi = #tab
	end
	
	if low < 1 or low > #tab or hi < 1 or hi > #tab then
		return nil, "INVALID STARTING/ENDING POINT"
	end
	
	local distance = math.abs(hi - low)
	local step = math.floor( distance / 2 )
	local mid = low + step
	
	while distance > 0 do
		if cmp( tab[mid], val ) then
			low = mid+1
			distance  = distance - (step+1)
		else
			distance = step
		end
		step = math.floor( distance / 2 )
		mid = low + step
	end
	
	-- If there's no value being greater or equal than val then return the last index +1
	if cmp( tab[low], val ) then
		return low+1
	end
	
	return low, tab[low]
end

-- Checks if the first arrays contain all of the elements of the second array
function utilities.containsAll( arr1, arr2 )
	local flag
	
	if arr1 and type(arr1) ~= "table" then
		arr1 = {arr1}
	end
	if arr2 and type(arr2) ~= "table" then
		arr2 = {arr2}
	end
	
	for i = 1, #arr2 do
		flag = false
		for j = 1, #arr1 do
			if arr1[j] == arr2[i] then
				flag = true
			end
		end
		if not flag then	return false	end
	end
	return true
end

-- Checks if the both arrays have at least one common element
function utilities.containsAny( arr1, arr2 )
	if arr1 and type(arr1) ~= "table" then
		arr1 = {arr1}
	end
	if arr2 and type(arr2) ~= "table" then
		arr2 = {arr2}
	end
	
	if not arr1 then return nil end
	if not arr2 then return arr1 end
	
	for i = 1, #arr2 do
		for j = 1, #arr1 do
			if arr1[j] == arr2[i] then
				return true
			end
		end
	end
	return false
end

function utilities.getTableField( t, field )
	if type(t) ~= "table" or type(field) ~= "string" then
		return nil
	end
	
	local fields = utilities.split(field, "%.")
	local value = t
	
	for i = 1, #fields do
		if type(value) ~= "table" then
			return nil, "FIELD NOT FOUND"
		end
		value = value[ fields[i] ]
	end
	
	return value
end

-- Took from the lua-users page: http://lua-users.org/wiki/CopyTable
function utilities.deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[utilities.deepcopy(orig_key)] = utilities.deepcopy(orig_value)
        end
        setmetatable(copy, utilities.deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

return utilities
