-- libs
local luajit_request = require 'luajit-request'
local cjson = require 'cjson'; cjson.encode_sparse_array(false, 0) -- read lua-cjson docs :)
local luadate = require 'date'; luadate.fmt("%D %H:%M:%S") -- set the default presentation format

local util = require('Utilities')
local Instances = Instances -- This avoids messing the global variable itself a bit

local GeneralStuff = require('class'):new()
GeneralStuff.new = nil

-- Load the configuration file and made it's settings available for others
GeneralStuff.configuration = require('config')

-- Load the database and provide access to database connections to others
GeneralStuff.database = require('DB')

--GeneralStuff.SLACKTEAMURL = "https://hooks.slack.com/services/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"
GeneralStuff.SLACKTEAMURL = "localhost:9999"
GeneralStuff.SLACKAPIURL = "https://slack.com/api"
GeneralStuff.TOKEN = "xoxp-10522107940-10536035312-12886577955-3187f9ba9a"
GeneralStuff.DEFAULTCHANNEL = "#bot-playground"
GeneralStuff.HELPTEXT = nil

local function generateHelp()
	local str = {}
	table.insert( str, "Available commands:" )
	for _, v in pairs(Instances.COMMANDS) do
		table.insert( str, v.HELPTEXT:match("[^\n]+") ) --First line from help text
	end
	
	return table.concat( str, '\n')
end

function GeneralStuff.execute( cmd, map )
	
	if cmd == nil or type(cmd) ~= "string" then
		return
	end
	
	if Instances.COMMANDS[ cmd ] then
		local execution = Instances.COMMANDS[cmd]:new( map )
		execution:Run()
	else
		if not GeneralStuff.HELPTEXT then
			GeneralStuff.HELPTEXT = generateHelp()
		end
		map.text = GeneralStuff.HELPTEXT
		GeneralStuff.sendMessage( GeneralStuff.forgeMessage( map ) )
	end
	
end

function GeneralStuff.getArguments( str )
	
	local group_regex = "([-][-].-)[-][-]"
	local group_regex_end = "([-][-].-)$"
	local group_key_regex = "([-][-][^ ]+)"
	local group_val_regex = " (.+)$"
	local arguments = { [0] = 0 }
	local step
	
	if str == nil then return arguments end
	if type(str) ~= "string" then return arguments end
	if str:len() <= 0 then return arguments end
	
	str = util.trim(str)
	while str:len() > 0 do
		
		local group = str:match( group_regex )
		if group == nil then 
			group = str:match( group_regex_end )
		end
		
		if group ~= nil then
			local key = group:match( group_key_regex )
			local val = group:match( group_val_regex ) or true
			
			if type(val) == "string" then
				val = util.trim(val)
			end
			
			if key then
				arguments[ key ] = val
				arguments[0] = arguments[0] + 1
			end
		end
			
			if group == nil then
				step = str:find(" ") or 1
			else	
				step = group:len()
			end
			
			str = str:sub( step + 1 )
			str = util.trim(str)
	end
	
	return arguments
end

function GeneralStuff.sendMessage( msgMap, custom_url )
	
	if not msgMap then
		return nil
	end
	
	local url = GeneralStuff.SLACKTEAMURL
	if type(custom_url) == "string" then
		url = custom_url
	end
	
	local first = true
	local data = {"payload={"}
	
	for k, v in pairs(msgMap) do
		if first then 
			first = false
		else 
			table.insert( data, "," )
		end
		
		table.insert( data, '"' )
		table.insert( data, k )
		table.insert( data, '":"' )
		table.insert( data, v )
		table.insert( data, '"' )
	end
	table.insert( data, "}" )
	
	local response = luajit_request.send(
		url, {  ["method"] = "POST", ["data"] = table.concat(data)  }
	)
	
	return response
end

function GeneralStuff.forgeMessage( msgMap )

	local chanName = GeneralStuff.getChannelName( msgMap )	
	local parametersTranslate = {
		["channel_name"] = "channel",
		["channel_id"] = "channel_id",
		["user_name"] = "user",
		["user_id"] = "user_id",
		["text"] = "text",
	}
	
	local keyName
	for k, v in pairs(msgMap) do
		keyName = parametersTranslate[k]
		
		if keyName then
			msgMap[k] = nil
			msgMap[keyName] = v
		end
		
	end
	
	msgMap.channel = chanName
	return msgMap
end

function GeneralStuff.getChannelName( msgMap )
	
	if not msgMap then
		return ""
	end
	
	local chan
	
	if msgMap.channel_name and msgMap.channel_name ~= "privategroup" then
		chan = "#" .. msgMap.channel_name
	elseif msgMap.channel_id then
		
		local url =	table.concat{
			GeneralStuff.SLACKAPIURL, 
			"/groups.info?token=",
			GeneralStuff.TOKEN, 
			"&channel=", 
			msgMap.channel_id
		}
		
		local response = GeneralStuff.getResponseObject( url )
		
		if not response then
			chan = GeneralStuff.DEFAULTCHANNEL
		else
			chan = "#" .. response.group.name
		end
	end	
	
	if not chan and msgMap.user_name then
		chan = "@" .. msgMap.user_name
	elseif not chan and msgMap.user_id then
		
		local url =	table.concat{
			GeneralStuff.SLACKAPIURL,
			"/users.info?token=",
			GeneralStuff.TOKEN,
			"&user=",
			msgMap.channel_id
		}
		
		local response = GeneralStuff.getResponseObject( url )
		
		if not response then
			chan = GeneralStuff.DEFAULTCHANNEL
		else
			chan = "@" .. response.user.name
		end
		
	end
	
	return chan
end

function GeneralStuff.jsonToObject( jsonString )
	local json = cjson.new()
	if type(jsonString) ~= "string" then
		return nil
	end
	
	return json.decode( jsonString )
end

function GeneralStuff.objectToJson( object )
	local json = cjson.new()
	if not object then
		return "{}"
	end
	
	return json.encode( object )
end

function GeneralStuff.getResponseObject( url )
	local json = cjson.new()
	local response = luajit_request.send( url )
	
	if not response then
		return nil, "COULD NOT SEND REQUEST"
	elseif response.code ~= "200" then
		return json.decode(response.body), "error code received", response.code
	end
	
	return json.decode(response.body)
end

function GeneralStuff.newDatabaseAccess()
	return GeneralStuff.database:new( GeneralStuff.configuration )
end

return GeneralStuff
