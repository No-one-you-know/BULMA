local GS = require('GeneralStuff')
local util = require('Utilities')
local Bot = require('class'):new()

function Bot:work( headersMap )
	if not headersMap then return end
	
	local text = headersMap.text
	if not text then 
		return 
	else
		text = util.trim(text)
	end
	
	local command = string.match(text, "([^ ]+)")
	if not command then return end
	
	command = command:lower()	-- Lowercase command
	text = string.sub(text, string.len(command) + 1)	-- The rest of the text
	
	-- Trim extra spaces and tabs
	headersMap.text = util.trim(text)
	
	GS.execute( command, headersMap )	-- Call the command
end

function Bot:TEST()
	
	local headersMap = {
		text = "",
		channel_name = "bot-playground",
		channel_id = "ZZZZZZZZ",
		user_name = "NEIN"
	}
	
	Bot:work( headersMap )
	
end

return Bot
