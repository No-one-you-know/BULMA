Instances = {}

local lfs = require 'lfs'

Instances.JUDGES = {}
for filename in lfs.dir('Judges') do
	filename = filename:match( "^(.-)%.lua$" )
	if filename and filename ~= "GeneralJudge" then
		Instances.JUDGES[filename:lower()] = require( filename )
	end
end

-- shortcut for codeforces
Instances.JUDGES["cf"] = Instances.JUDGES["codeforces"]

Instances.COMMANDS = {}
for filename in lfs.dir('Behavior') do
	filename = filename:match( "^(.-)%.lua$" )
	if filename and filename ~= "GeneralBehavior" then
		Instances.COMMANDS[filename:lower()] = require( filename )
	end
end

return Instances
